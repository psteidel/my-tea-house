import MainPage from "../components/MainPage";
import { Box, Text } from "grommet";
import TeaSearch from '../components/TeaSearch'
import { UserPanel } from "../components/Login";


export default function Index() {

  return (
    <MainPage>
      <Box direction="row" pad="large" gap="large">
        <Box flex="grow">
          <Text>Hello</Text>
        </Box>
        <Box>
          <UserPanel />
        </Box>
      </Box>
    </MainPage>
  );
}