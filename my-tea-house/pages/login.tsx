import MainPage from '../components/MainPage';
import { Box, Heading } from 'grommet';
import { useRouter } from 'next/router';
import { useUser } from '../components/User'
import { LoginForm, RegisterForm } from '../components/Login'


export default function Login() {
    const { user } = useUser()
    const router = useRouter();
    if (user.loggedIn) {
        router.replace('/');
    }

    return (
        <MainPage>
            <Box direction="row" gap="xlarge" alignSelf="center">
                <Box align="center">
                    <Heading level="3">Login</Heading>
                    <LoginForm />
                </Box>
                <Box align="center">
                    <Heading level="3">Register</Heading>
                    <RegisterForm />
                </Box>
            </Box>
        </MainPage>
    );
}