import { InMemoryCache, ApolloClient, ApolloProvider, createHttpLink } from '@apollo/client';
import { Grommet } from 'grommet';
import "../reset.css";
import { setContext } from '@apollo/client/link/context';
import { UserProvider } from '../components/User';

const colors = {
  brand: "#00c853",
  dark: "#16141b",
  light: "#f3e9ec",
  'status-error': 'red'
};

const theme = {
    global: {
      font: {
        family: 'Roboto, sans-serif',
        size: '18px',
        height: '20px'
      },

      colors: colors
    }
  };

  const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem("token")
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ""
      }
    }
  });

  const httpLink = createHttpLink({ uri: "http://localhost:4000/api" })

  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
  })

export default function App({ Component, pageProps }) {
  return (
    <Grommet theme={theme}>
      <ApolloProvider client={client}>
        <UserProvider>
        <Component {...pageProps} />
        </UserProvider>
      </ApolloProvider>
    </Grommet>
  );
}
