import { useQuery, gql } from '@apollo/client';
import MainPage from '../components/MainPage';
import { Grid, Heading, Box, Text, Image } from 'grommet';
import { useState } from 'react';

const TEAS = gql`query GetTeas { teas { id, name, images, vendor { name }, rating }}`;

function Tea({ id, name, images, vendor, rating }) {
    const [activeImage, setActiveImage] = useState(images && images[0]);
    return (
        <Box elevation="medium" pad="small"background={"lightblue"} justify="between">
            <Box>
                <Heading level="3" size="xsmall">
                    {name}
                </Heading>
                <Box>
                    {vendor ? <Text color="grey">{vendor.name}</Text> : []}
                </Box>
            </Box>
            <Box>
                <Box height="medium">
                    <Image fit="cover" src={`/images/${activeImage}`} />
                </Box>
                <Box margin={{ "top": "small" }} direction="row" gap="small" height="xsmall">
                    {images.slice(0, 4).map(i =>
                        <Box
                            round="full"
                            overflow="hidden"
                            width="xsmall"
                            height="xsmall"
                            key={i}
                            onClick={() => setActiveImage(i)}
                            border={i == activeImage
                                ? { color: "brand", size: "small" }
                                : { color: "grey", size: "small" }}>
                            <Image fit="cover" src={`/images/${i}`} />
                        </Box>)}
                </Box>
            </Box>
        </Box>)
}

function TeaList() {
    const { loading, error, data } = useQuery(TEAS);
    if (loading) return <div />;
    if (error) return <p>Error: {error.message}</p>;
    return (
        <Grid gap="medium" columns={{ "count": "fit", "size": "medium" }}>
            {data && data.teas.map(tea =>
                <Tea key={tea.id} {...tea} />)
            }
        </Grid>
    );
}

export default function TeaPage() {
    return (
        <MainPage>
            <TeaList />
        </MainPage>
    );
}
