import { Box } from 'grommet';
import { Star } from 'grommet-icons';

export default function Stars(props){
    return  <Box>{Array(props.amount).map(i => <Star key={i} />)}</Box>
}