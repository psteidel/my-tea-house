import { useState } from "react";
import { useMutation, gql } from "@apollo/client";
import { InProgress, User as UserIcon, Lock, Checkmark } from "grommet-icons";
import { Form, FormField, TextInput, Box, Button, Text, Tabs, Tab } from "grommet";
import { useUser, User } from "./User";

interface LoginQuery {
    login?: { token: String }
}

const LOGIN = gql`mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
        token
    }
}`;

export function LoginForm() {
    const [value, setValue] = useState({ email: "", password: "" })
    const { login } = useUser();

    const [queryLogin, { loading, error }] = useMutation(LOGIN, {
        onCompleted: ({ login: l }: LoginQuery) => {
            if (l) {
                login({ token: l.token, name: value.email })
            }
        },
        errorPolicy: 'all'
    });

    function query(v: { email: String, password: String }) {
        queryLogin({
            variables: { email: v.email, password: v.password },
        })
    }

    if (loading) {
        return <InProgress />
    }

    return (
        <Form
            value={value}
            onChange={(v) => setValue(v)}
            onSubmit={(event) => query(event.value)}
        >
            <FormField name="email" htmlFor="email" label="Email">
                <TextInput type="text" id="email" name="email" icon={<UserIcon />}></TextInput>
            </FormField>
            <FormField name="password" htmlFor="password" label="Password">
                <TextInput type="password" id="password" name="password" icon={<Lock />}></TextInput>
            </FormField>
            <Box direction="row" gap="medium">
                <Button type="submit" primary label="Login" />
            </Box>
            {error ? <Box><Text color="status-error">{error.message}</Text></Box> : []}
        </Form>
    )
}

interface RegisterQuery {
    register?: { id: Number }
}

const REGISTER = gql`mutation Register($email: String!, $password: String!) {
    register(email: $email, password: $password){ 
        id
    }
}`;

export function RegisterForm() {
    const [formValue, setFormValue] = useState({ email: "", password: "", confirmPassword: "" })
    const [registered, setRegistered] = useState<Number>(null);
    const [queryRegister, { loading, error }] = useMutation(REGISTER, {
        onCompleted: ({ register }: RegisterQuery) => {
            if (register) { setRegistered(register.id) }
        },
        errorPolicy: 'all'
    })

    function query(v: any) {
        queryRegister({
            variables: { email: v["email"], password: v["password"] }
        })

    }

    if (loading) {
        return <InProgress />
    }

    if (registered) {
        return <Text>You have registered for: {formValue.email}</Text>
    }

    return (
        <Form
            value={formValue}
            onChange={(v) => setFormValue(v)}
            onSubmit={(event) => query(event.value)}
        >
            <FormField validate={[() => { if (error) { return error.message } }, {
                regexp: /.+@.+/,
                message: "Please input an Email",
            }]} name="email" htmlFor="email" label="Email" >
                <TextInput type="text" id="email" name="email" icon={<UserIcon />}></TextInput>
            </FormField>
            <FormField name="password" htmlFor="password" label="Password">
                <TextInput type="password" id="password" name="password" icon={<Lock />}></TextInput>
            </FormField>
            <FormField validate={(_, form) => {
                if (form['password'] !== form['confirmPassword']) {
                    return "Passwords Must Match";
                }
            }} name="confirmPassword" htmlFor="confirmPassword" label="Confirm Password">
                <TextInput type="password" id="confirmPassword" name="confirmPassword" icon={<Checkmark />}></TextInput>
            </FormField>
            <Box direction="row" gap="medium">
                <Button type="submit" primary label="Register" />
            </Box>
        </Form>
    );
}

export function UserPanel() {
    const { user } = useUser();

    return (
        <Box>
            {user.loggedIn
                ? <Box alignSelf="center">
                    <Text>Welcome!!</Text>
                </Box>
                :
                <Tabs alignControls="stretch" alignSelf="stretch">
                    <Tab title="Login">
                        <LoginForm />
                    </Tab>
                    <Tab title="Register">
                        <RegisterForm />
                    </Tab>
                </Tabs>
            }
        </Box>
    )
}