import { Header, Box, Heading } from 'grommet';
import NavBar from './Navbar';
import { ReactNode, useEffect } from 'react';
import TeaSearch from './TeaSearch';

export default function MainPage(props: { title?: string; children: ReactNode }) {
    const title = props.title || "mytea.house"
    useEffect(() => {window.document.title = title}, [])
    return (
        <Box>
            <Header pad="xsmall" background="brand">
                <Heading size="1" color="light">{props.title || "mytea.house"}</Heading>
                <Box flex="grow">
                    <TeaSearch />
                </Box>
                <NavBar />
            </Header>
            <Box as="main" pad="large">
                {props.children}
            </Box>
        </Box>
    );
}
