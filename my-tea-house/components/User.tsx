import { createContext, useContext, useState, SetStateAction, useEffect } from 'react';

function getCachedUser(): User {
    const cached = localStorage.getItem('user')
    if (cached) {
        const parsed = JSON.parse(cached)
        const userBase: UserBase = {
            name: parsed["name"],
            token: parsed["token"]
        }
        if (typeof userBase.name === 'undefined'
            || typeof userBase.token === 'undefined') {
            return {loggedIn: false};
        }
        return {loggedIn: true, ...userBase};
    }
    return {loggedIn: false}
}

function setCachedUser(user: User) {
    if (user.loggedIn) {
        localStorage.setItem('user', JSON.stringify({
            token: user.token,
            name: user.name
        }))
    } else {
        localStorage.removeItem('user');
    }
}

export interface UserBase {
    name: String
    token: String
}


export interface UserLoggedIn extends UserBase {
    loggedIn: true

}

export interface UserLoggedOut {
    loggedIn: false
}

export type User = UserLoggedIn | UserLoggedOut

const UserContext = createContext<{user: User; setUser: (u: User) => void}>({ 
    user: {loggedIn: false},
    setUser: null
})

export function useUser() {
    const {user, setUser} = useContext(UserContext)
    const login = (u: UserBase) => setUser({loggedIn: true, ...u});
    const logout = () => setUser({loggedIn: false});

    return {user, login, logout};
}

export function UserProvider(props) {
    const [user, setUser] = useState<User>({loggedIn: false})

    useEffect(() => {
        setUser(getCachedUser())
    }, [])

    useEffect(() => {
        setCachedUser(user)
    }, [user])

    return (
        <UserContext.Provider value={{user, setUser}}>
            {props.children}
        </UserContext.Provider>
    );
}