import { Box, TextInput, Text, Drop, ThemeContext } from 'grommet'
import { Search } from 'grommet-icons'
import { useState, useEffect, useRef, Ref } from 'react'

export default function TeaSearch() {
    const [search, setSearch] = useState<String>("")
    const inputRef = useRef(null);

    function doChange({ target }: { target: { value: String } }) {

        setSearch(target.value)
    }

    return (
        <Box>
            <ThemeContext.Extend value={{"global":{"colors": {"placeholder": "#ffffff"}}}}>
            <TextInput placeholder="Search Teas" 
                ref={inputRef} onChange={doChange} icon={<Search />} />
            <Drop target={inputRef.current} align={{ "top": "bottom" }}>{search}</Drop>
            </ThemeContext.Extend>
        </Box>
    )
}