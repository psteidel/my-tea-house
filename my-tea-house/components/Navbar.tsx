import { Button, Nav, Anchor } from 'grommet';
import { Home, User, Login, Logout } from "grommet-icons";
import Link from 'next/link';
import { useUser } from './User';

export default function Navbar() {
    const {user, logout } = useUser();

    return (
        <Nav direction="row">
            <Link href="/"><Anchor color="light" icon={<Home />} /></Link>
            {user.loggedIn
                ? [ 
                    <Link key="login" href="/user/"><Anchor color="light" icon={<User />} /></Link>,
                    <Button key="logout" onClick={logout} icon={<Logout color="light" />} />
                ] : <Link href="/login/"><Anchor color="light" icon={<Login />} /></Link>
            }
        </Nav>
    );
};