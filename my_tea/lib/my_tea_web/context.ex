defmodule MyTeaWeb.Context do
  @behaviour Plug
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _) do
    case build_context(conn) do
      {:ok, context} -> put_private(conn, :absinthe, %{context: context})
    end
  end

  def build_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
         {:ok, current_user} <- authorize(token) do
      {:ok, current_user}
    else
      _ -> {:ok, %{}}
    end
  end

  defp authorize(token) do
    with {:ok, id} <- Phoenix.Token.verify(
           MyTeaWeb.Endpoint,
           "user auth",
           token,
           max_age: 86400
         ),
         user when user != nil <- MyTea.Repo.User.by_id(id) do
      {:ok, user}
    else
      _ -> {:error, "Invalid token"}
    end
  end
end
