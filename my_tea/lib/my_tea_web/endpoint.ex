defmodule MyTeaWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :my_tea

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  # @session_options [
  #   store: :cookie,
  #   key: "_my_tea_web_key",
  #   signing_salt: "ZkriqES2"
  # ]

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug Phoenix.CodeReloader
    plug Phoenix.Ecto.CheckRepoStatus, otp_app: :my_tea
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug CORSPlug
  plug MyTeaWeb.Router
end
