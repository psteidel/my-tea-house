defmodule MyTeaWeb.Schema.Types do
	use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  object :tea do
    field :id, :id
    field :name, :string
    field :type, :string
    field :vendor, :vendor do
      resolve(dataloader(MyTea.Tea))
    end
    field :description, :string
    field :images, list_of(:string)
    field :rating, :integer
  end

  object :vendor do
    field :id, :id
    field :name, :string
    field :teas, list_of(:tea) do
      resolve(dataloader(MyTea.Tea))
    end
  end

  object :session do
    field :token, :string
    field :user, :user
  end

  object :user do
    field :id, :id
    field :username, :string
    field :avatar, :string
  end
end
