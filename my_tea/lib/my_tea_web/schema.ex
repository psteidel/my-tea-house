defmodule MyTeaWeb.Schema do
  use Absinthe.Schema
  alias MyTeaWeb.Resolvers
  alias MyTea.Tea

  def context(ctx) do
    loader =
      Dataloader.new()
      |> Dataloader.add_source(Tea, MyTea.Tea.data())

    Map.put(ctx, :loader, loader)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader] ++ Absinthe.Plugin.defaults()
  end

  import_types(MyTeaWeb.Schema.Types)

  query do
    @desc "Get all teas"
    field :teas, list_of(:tea) do
      resolve &Resolvers.Teas.list/3
    end

    field :vendors, list_of(:vendor) do
      resolve &Resolvers.Vendors.list/3
    end

    @desc "Get tea by id"
    field :tea, :tea do
      arg :id, non_null(:id)
      resolve &Resolvers.Teas.find_tea/2
    end
  end

  mutation do
    @desc "Log in"
    field :login, type: :session do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      resolve(&Resolvers.Users.login/3)
    end

    @desc "Register User"
    field :register, type: :user do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      resolve(&Resolvers.Users.register/3)
    end
  end
end
