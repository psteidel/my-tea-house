defmodule MyTeaWeb.Resolvers.Users do
  def login(_parent, %{email: email, password: password}, _context) do
    with {:ok, %MyTea.Repo.User{id: id}} <- MyTea.Repo.User.authenticate(email, password),
         token <- Phoenix.Token.sign(MyTeaWeb.Endpoint, "user auth", id) do
      {:ok, %{token: token}}
    end
  end

  def register(_parent, %{email: email, password: password}, _context) do
    MyTea.Repo.User.register(email, password)
  end

  def resolve_user(_parent, _args, %{context: %{user_id: user_id}}) do
    {:ok, MyTea.Repo.User.by_id(user_id)}
  end
  def resolve_user(_parent, _args, _context), do: {:ok, nil}
end
