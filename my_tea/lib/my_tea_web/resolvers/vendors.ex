defmodule MyTeaWeb.Resolvers.Vendors do
  def list(_parent, _args, _context) do
    {:ok, MyTea.Repo.Vendor.list()}
  end
end
