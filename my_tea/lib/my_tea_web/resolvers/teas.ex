defmodule MyTeaWeb.Resolvers.Teas do
  def list(_parent, _args, _resolution) do
    {:ok, MyTea.Repo.Tea.list()}
  end

  def find_tea(%{id: id}, _resolution) do
    case MyTea.Repo.Tea.by_id(id) do
      nil -> {:error, "Tea ID #{id} not found"}
      tea -> {:ok, tea}
    end
  end
end
