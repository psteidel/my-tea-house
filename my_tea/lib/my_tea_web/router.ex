defmodule MyTeaWeb.Router do
  use MyTeaWeb, :router

  pipeline :graphql do
    plug :accepts, ["json"]
    plug MyTeaWeb.Context
  end

  scope "/api" do
    pipe_through :graphql
    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: MyTeaWeb.Schema
    forward "/", Absinthe.Plug,
      schema: MyTeaWeb.Schema
  end
end
