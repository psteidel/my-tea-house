defmodule MyTea.DataLoader do
  def load_json_file(vendor, file) do
    {:ok, file} = File.read(file)

    %MyTea.Repo.Vendor{id: id} =
      case MyTea.Repo.Vendor.by_name(vendor) do
        nil -> MyTea.Repo.Vendor.create!(vendor)
        rec -> rec
      end

    decoded =
      file
      |> String.split("\n")
      |> Enum.map(&Jason.decode/1)

    tasks =
      for {:ok, json} <- decoded do
        Task.async(fn ->
          image_urls = json["images"]

          images =
            for image when is_binary(image) <- image_urls do
              {:ok, %HTTPoison.Response{body: body}} = HTTPoison.get(image)
              hash = :crypto.hash(:sha, body)
              name = "#{Base.encode32(hash)}.jpg"
              File.write("static/#{name}", body)
              name
            end

          MyTea.Repo.Tea.insert(%MyTea.Repo.Tea{
            name: json["title"],
            description: json["description"],
            images: images |> Enum.filter(&is_binary/1),
            vendor_id: id
          })
        end)
      end

    tasks
    |> Enum.each(&Task.await/1)
  end
end
