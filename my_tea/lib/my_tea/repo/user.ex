defmodule MyTea.Repo.User do
  alias MyTea.Repo

  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :password, :string

    timestamps()
  end

  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password])
    |> validate_required([:email, :password])
  end

  def by_id(id), do: Repo.get_by(MyTea.Repo.User, id: id)

  def by_email(email), do: Repo.get_by(MyTea.Repo.User, email: email)

  def authenticate(email, password) do
    case by_email(email) do
      nil ->
        {:error, "Invalid user/password"}

      user = %MyTea.Repo.User{password: hash} ->
        if Argon2.verify_pass(password, hash) do
          {:ok, user}
        else
          {:error, "Invalid user/password"}
        end
    end
  end

  def register(email, password) do
    case by_email(email) do
      %MyTea.Repo.User{} ->
        {:error, "User already exists"}

      nil ->
        %{password_hash: hash} = Argon2.add_hash(password)
        user = %MyTea.Repo.User{email: email, password: hash}
        Repo.insert(user)
    end
  end
end
