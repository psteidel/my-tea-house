defmodule MyTea.Repo.TeaType do
	def list() do
    ["white", "black", "green", "oolong", "pu-er", "dark", "herbal"]
  end
end
