defmodule MyTea.Repo.Tea do
  import Ecto.Query
  alias MyTea.Repo
  use Ecto.Schema
  import Ecto.Changeset

  schema "teas" do
    field :name, :string
    field :type, :string
    field :images, {:array, :string}
    field :description, :string
    field :rating, :integer
    belongs_to :vendor, MyTea.Repo.Vendor
    timestamps()
  end

  def changeset(tea, attrs) do
    tea
    |> cast(attrs, [:name, :type])
    |> validate_required([:name, :type])
  end

  def list() do
    Repo.all(from t in MyTea.Repo.Tea)
  end

  def by_id(id) do
    Repo.get_by(MyTea.Repo.Tea, id: id)
  end

  def insert(rec), do: Repo.insert(rec)

  @deprecated "Please use `insert/1`"
  def create(name, type) do
    type = String.downcase(type)
    if(not Enum.member?(MyTea.Repo.TeaType.list(), type)) do
      {:error, "No such type #{type}"}
    else
      Repo.insert(%MyTea.Repo.Tea{name: name, type: type})
    end
  end
end
