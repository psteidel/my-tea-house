defmodule MyTea.Repo.Vendor do
  use Ecto.Schema
  import Ecto.Query, [:from]
  alias MyTea.Repo
  import Ecto.Changeset

  schema "vendors" do
    field :name, :string
    has_many :teas, MyTea.Repo.Tea

    timestamps()
  end

  @doc false
  def changeset(vendor, attrs) do
    vendor
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def by_name(name), do: Repo.get_by(__MODULE__, name: name)
  def create!(name), do: Repo.insert!(%__MODULE__{name: name})

  def list(), do: Repo.all(from(t in __MODULE__))
end
