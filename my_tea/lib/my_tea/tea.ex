defmodule MyTea.Tea do
    def data() do
      Dataloader.Ecto.new(MyTea.Repo, query: &query/2)
    end

    def query(queryable, _params) do
      queryable
    end
end
