defmodule MyTea.Repo.Migrations.CreateVendors do
  use Ecto.Migration

  def change do
    create table(:teas) do
      add :name, :string
      add :type, :string
      add :images, {:array, :string}
      add :description, :text
      add :vendor_id, :id

      timestamps()
    end

    create table(:users) do
      add :email, :string
      add :password, :string

      timestamps()
    end

    create table(:vendors) do
      add :name, :string

      timestamps()
    end
  end
end
