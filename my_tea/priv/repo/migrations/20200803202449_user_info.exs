defmodule MyTea.Repo.Migrations.UserInfo do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :avatar, :string
      add :username, :string
    end
  end
end
