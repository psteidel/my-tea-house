defmodule MyTea.Repo.Migrations.TeaRating do
  use Ecto.Migration

  def change do
    alter table(:teas) do
      add :rating, :int, default: 0
    end
  end
end
